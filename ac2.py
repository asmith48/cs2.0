from Crypto.Cipher import AES
import base64

key = "neurotransmitter"
key = key.encode('iso-8859-15')
msg = ''
with open('ciphertexts2.txt', 'r') as f:
   line = f.readline().strip()
   while len(line)>1:
      msg = msg + line
      line = f.readline().strip()
 
decipher = AES.new(key, AES.MODE_ECB)
print(decipher.decrypt(base64.b64decode(msg)))


