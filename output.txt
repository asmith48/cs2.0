We're going inside of 'em, we're going outside of 'em -- inside of 'em! outside of 'em! --
and when we get them on the run once, we're going to keep ‘em on the run. And we're not going
to pass unless their secondary comes up too close. But don't forget, men -- we're gonna get
'em on the run, we're gonna go, go, go, go! -- and we aren't going to stop until we go over
that goal line! And don't forget, men -- today is the day we're gonna win. They can't lick us
-- and that's how it goes... The first platoon men -- go in there and fight, fight, fight,
fight, fight! What do you say, men!

