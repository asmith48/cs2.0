import random
import string
import base64
from Crypto.Cipher import AES

#used https://stackoverflow.com/questions/20852664/python-pycrypto-encrypt-decrypt-text-files-with-aes code for pad function and for ideas on use of bytes
def pad(s):
    return s + b"\0" * (AES.block_size - len(s) % AES.block_size)

def AESKey():
   char = 'abcdef0123456789'
   keyS = ''
   for i in range(16):
      index = random.randint(0,15)
      keyS = keyS + char[index]
   keyB = keyS.encode('iso-8859-15') #bytes(keyS, 'utf8')
   #print pad(keyB)
   return pad(keyB)

def encrypt(key, plaintext):
   start = 'a1c3d'
   end = 'a2dfd6945e'
   plaintext = start + plaintext + end
   plaintext = pad(plaintext)

   mode = random.randint(0,1)
   iv = '                '
   if mode==0: #ECB
      encryptType = "ecb"
      cipher = AES.new(key, AES.MODE_ECB)
      ciphertext = cipher.encrypt(plaintext)

   else: #CBC
      encryptType = "cbc"
      iv = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(16)) #used https://stackoverflow.com/questions/2257441/random-string-generation-with-upper-case-letters-and-digits-in-python 
      cipher = AES.new(key, AES.MODE_CBC, iv)
      ciphertext = cipher.encrypt(plaintext)

   #print "ciphertext: ", iv + ciphertext
   return iv + ciphertext, encryptType

def decrypt(key, ctext, eType):
   #attempt ecb decoding
   decipher = AES.new(key, AES.MODE_ECB)
   plaintextECB = decipher.decrypt(ctext[AES.block_size:])
   #plaintextECB = plaintextECB.rstrip(b"\0")[5:-10]
   #print "P ecb:",plaintextECB.rstrip(b"\0")[5:-10]

   #attempt cbc decoding
   iv = ctext[:AES.block_size]
   decipher2 = AES.new(key, AES.MODE_CBC, iv)
   plaintextCBC = decipher2.decrypt(ctext[AES.block_size:])
   #plaintextCBC = plaintextCBC.rstrip(b"\0")[5:-10]
   #print "P cbc:",plaintextCBC.rstrip(b"\0")[5:-10]

   #detect block cipher mode
   s1 = 0
   s2 = 32
   ctexts = []
   ctext = ctext[AES.block_size:]
   ctext = ''.join([c.encode("hex") for c in ctext])
   
   while s2 <= len(ctext):
      ctexts.append(ctext[s1:s2])
      s1 = s1+32
      s2 = s2+32
  
   #print ctexts 
   eFound = "cbc"
   seen = []
   for c in ctexts:
      if c in seen: 
         eFound = "ecb"
         break
      seen.append(c)

   if (eFound=="ecb" and eType=="ecb") or (eFound=="cbc" and eType=="cbc"):
      print eFound, "Correct"
   else: 
      print eFound, "Incorrect"

if __name__ == '__main__':
   for i in range(30):
      key = AESKey()

      msg = ''
      with open('sample.txt', 'r') as f:
         line = f.readline().strip()
         while len(line)>1:
            msg = msg + line
            line = f.readline().strip()

      ciphertext, eType = encrypt(key, msg);
      decrypt(key, ciphertext, eType)












